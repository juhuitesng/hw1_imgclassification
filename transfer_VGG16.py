""" transfer the pre-trained model on the class-provided dataset """


from MixAug import MixupImageDataGenerator
import histeq
import sys
import os
import pandas as pd
import numpy as np
from scipy.ndimage import gaussian_filter
from scipy.ndimage import filters
import matplotlib.pyplot as plt
from sklearn.metrics import confusion_matrix
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import LabelEncoder
from keras import optimizers
from keras.initializers import RandomUniform, glorot_uniform
from keras.callbacks import ModelCheckpoint, EarlyStopping, ReduceLROnPlateau
from keras.preprocessing.image import ImageDataGenerator
from keras.applications.inception_resnet_v2 import InceptionResNetV2
from keras.applications.densenet import DenseNet169
from keras.applications.vgg19 import VGG19
from keras.applications.vgg16 import VGG16
from keras.layers import Conv2D, MaxPooling2D, AveragePooling2D, GlobalAveragePooling2D
from keras.layers import Dense, Dropout, Flatten, Input, BatchNormalization, Conv2D
from keras.models import Sequential, Model, load_model
from keras.utils import to_categorical
from keras.preprocessing import image
import keras
print("vgg16_extra_pretrain ep50")

root = '/home/juhui/HW1/cs-ioc5008-hw1/dataset/dataset/'
train_dir = root + 'train/'
test_dir = root + 'test/'
model_dir = root + 'model/'

IMG_SIZE = 256
batch_size = 64

class_lst = [
    'bedroom', 'coast', 'forest', 'highway', 'insidecity',
    'kitchen', 'livingroom', 'mountain', 'office', 'opencountry',
    'street', 'suburb', 'tallbuilding',
]


def plot_model(model_details, model_name):

    # Create sub-plots
    fig, axs = plt.subplots(1, 2, figsize=(15, 5))

    # Summarize history for accuracy
    axs[0].plot(range(1, len(model_details.history['accuracy'])+1),
                model_details.history['accuracy'])
    axs[0].plot(range(1, len(model_details.history['val_accuracy'])+1),
                model_details.history['val_accuracy'])
    axs[0].set_title('Model Accuracy')
    axs[0].set_ylabel('Accuracy')
    axs[0].set_xlabel('Epoch')
    axs[0].set_xticks(np.arange(1, len(model_details.history['accuracy'])+1),
                      len(model_details.history['accuracy'])/10)
    axs[0].legend(['train', 'val'], loc='best')

    # Summarize history for loss
    axs[1].plot(range(1, len(model_details.history['loss'])+1),
                model_details.history['loss'])
    axs[1].plot(range(1, len(model_details.history['val_loss'])+1),
                model_details.history['val_loss'])
    axs[1].set_title('Model Loss')
    axs[1].set_ylabel('Loss')
    axs[1].set_xlabel('Epoch')
    axs[1].set_xticks(np.arange(
        1, len(model_details.history['loss'])+1), len(model_details.history['loss'])/10)
    axs[1].legend(['train', 'val'], loc='best')

    plt.savefig(model_dir + model_name + '.png')


# class_weights
labels_count = dict()
for img_class in [ic for ic in os.listdir(train_dir) if ic[0] != '.']:
    item = os.path.join(train_dir, img_class)
    if os.path.isdir(item):
        labels_count[img_class] = len(os.listdir(train_dir + img_class))
total_count = sum(labels_count.values())
class_weights = {cls: total_count / count for cls, count in
                 enumerate(labels_count.values())}


train_datagen = ImageDataGenerator(
    # rotation_range=40,
    # preprocessing_function=preprocess_input,
    rescale=1/255,
    width_shift_range=0.2,
    height_shift_range=0.2,
    shear_range=0.2,
    zoom_range=0.2,
    horizontal_flip=True,
    fill_mode='nearest',
    validation_split=0.2,
)
valid_datagen = ImageDataGenerator(
    rescale=1/255,
    # preprocessing_function=preprocess_input,
    validation_split=0.2,
)
test_datagen = ImageDataGenerator(
    rescale=1/255,
    # preprocessing_function=preprocess_input
)


train_generator = train_datagen.flow_from_directory(
    train_dir,
    target_size=(IMG_SIZE, IMG_SIZE),
    batch_size=batch_size,
    class_mode='categorical',
    subset='training',
    shuffle=True, seed=2,
)

validation_generator = valid_datagen.flow_from_directory(
    train_dir,
    target_size=(IMG_SIZE, IMG_SIZE),
    batch_size=56,
    class_mode='categorical',
    subset='validation',
    shuffle=False, seed=2,
)


model = load_model(model_dir + 'VGG16_extra_93293.h5')
print(model.summary())

# fine-tune only the dense layer
for layer in model.get_layer('vgg16').layers[:]:
    layer.trainable = False

# check trainable setting
layers = [[layer, layer.name, layer.trainable]
          for layer in model.get_layer('vgg16').layers]
print(pd.DataFrame(layers, columns=[
      'Layer Type', 'Layer Name', 'Layer Trainable']))

model.compile(loss='categorical_crossentropy', optimizer=optimizers.SGD(
    lr=0.0001, momentum=0.9), metrics=['accuracy'])
augmented_checkpoint = ModelCheckpoint(model_dir + 'vgg16_extra_pretrain.h5',
                                       monitor='val_accuracy',
                                       verbose=1,
                                       save_best_only=True,
                                       mode='auto'
                                       )

history = model.fit_generator(
    train_generator,
    steps_per_epoch=(len(train_generator.filenames)//batch_size)+1,  # 35,
    epochs=50,
    validation_data=validation_generator,
    validation_steps=10,  # 9
    class_weight=class_weights,
    callbacks=[augmented_checkpoint],
    verbose=1
)

plot_model(history, 'vgg16_extra_pretrain')
